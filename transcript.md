# Preparation
```javascript
function sha1_hex(text) {
var shasum = require('crypto').createHash('sha1');
shasum.update(text, 'utf-8');
return shasum.digest('hex');
}
function deflate(text, file) {
var fs = require('fs');
var defl = require('zlib').createDeflate();
fs.mkdirSync(require('path').dirname(file));
var out = fs.createWriteStream(file);
var input = new require('stream').Readable();
input._read = function noop() {};
input.push(text);
input.push(null);
input.pipe(defl).pipe(out);
}
```
# Presentation
```javascript
var blob = "Hello World!\n";
var content = "blob "+blob.length+"\0"+blob;
var sha1 = sha1_hex(content);
var path = '.git/objects/' + sha1.substr(0,2) + '/' + sha1.substr(2,sha1.length);
deflate(content, path);
```
```bash
$ ls -hl objects/
$ ls -hl objects/98/
$ git cat-file -p 980a0d5
```
```javascript
var store = function(content, type) {
	var stored = type+" "+content.length+"\0"+content;
	var sha1 = sha1_hex(stored);
	var path = '.git/objects/' + sha1.substr(0,2) + '/' + sha1.substr(2,sha1.length);
	deflate(stored, path);
	return sha1;
};
store("Hello World!!\n", "blob");

var tree = "100644 hi.txt\0"+"980a0d5f19a64b4b30a87d4206aade58726b60e3";
store(tree, "tree");

tree = "100644 hi.txt\0"+"980a0d5f19a64b4b30a87d4206aade58726b60e3";
tree += "100644 hii.txt\0"+"936977184a9fa89d82f86957a90b92d4924b6573";
store(tree, "tree");
```
